import a1 from "@/assets/avatar_1.png";
import a2 from "@/assets/avatar_2.png";
import a3 from "@/assets/avatar_3.png";
import a4 from "@/assets/avatar_4.png";
import a5 from "@/assets/avatar_5.png";
import a6 from "@/assets/avatar_6.png";
import a7 from "@/assets/avatar_7.png";
import a8 from "@/assets/avatar_8.png";
import a9 from "@/assets/avatar_9.png";
import a10 from "@/assets/avatar_10.png";
import a11 from "@/assets/avatar_11.png";
import a12 from "@/assets/avatar_12.png";

export default {
    1: a1,
    2: a2,
    3: a3,
    4: a4,
    5: a5,
    6: a6,
    7: a7,
    8: a8,
    9: a9,
    10: a10,
    11: a11,
    12: a12,
}