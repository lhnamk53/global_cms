import axios from "axios";

//Private
let apiUrl = 'https://authen.yeaz.win/apiCMS/api';
let app = null;

let global = {
  registerApp(vm) {
    app = vm;
  },
  async fetch(apiName, data = {}) {
    const jwt = localStorage.getItem("gcms:jwt") || "";
    return await axios(apiUrl, {
      method: 'post',
      headers: {
        'content-type': 'application/json',
        'Authorization': jwt
      },
      data: JSON.stringify({ api_name: apiName, data: data })
    }).then((res) => {
      const ret = res.data;
      if (ret.token) {
        localStorage.setItem("gcms:jwt", ret.token)
      }
      if (ret.status >= 0) {
        localStorage.setItem("gcms:last-action-ts", new Date().getTime());
      }
      return ret
    }).catch((err) => {
      return { status: -500, desc: err.message }
    });
  },
  loggedIn() {
    const json = localStorage.getItem("gcms:accInfo");
    if (!json) return false;
    try {
      const accInfo = JSON.parse(json);

      if (accInfo.date_expires < new Date().getTime()) {
        localStorage.removeItem("gcms:accInfo");
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  },
  logout() {
    localStorage.removeItem("gcms:accInfo");
    localStorage.removeItem("gcms:jwt");
    localStorage.removeItem("gcms:last-action-ts");
    location.reload();
  },
  getAccInfo() {
    const json = localStorage.getItem("gcms:accInfo");
    return JSON.parse(json);
  },
  async getListGame() {
    const json = sessionStorage.getItem("gcms:listgame");
    if (json) return JSON.parse(json);
    const res = await global.fetch("list_game");
    if (res.status < 0) {
      return null;
    }
    sessionStorage.setItem("gcms:listgame", JSON.stringify(res.data));
    return res.data;
  },
  moneyText(val) {
    if (val === 0) return '0';
    if (val == undefined) return '';
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  },
  formatDate(date) {
    if (!date) return null
    const [year, month, day] = date.split('-')
    return `${day}/${month}/${year}`
  },
  formatDateInt(date) {
    if (!date) return null
    const [year, month, day] = date.split('-')
    return `${year}${month}${day}`
  },
  parseDateInt(dateInt) {
    if (!dateInt) return null
    const date = dateInt + '';
    return `${date.substr(6, 2)}/${date.substr(4, 2)}/${date.substr(0, 4)}`
  },
  showNoti(dialogObj) {
    app.dialog = dialogObj;
  },
  hideNoti() {
    app.dialog.visible && (app.dialog.visible = false);
  },
  handleError(res) {
    if (res.status >= 0) return;

    if (res.status == -111) {
      global.showNoti({
        visible: true,
        header: "Error",
        text: res.desc,
        close: () => {
          global.logout();
        }
      });
      return;
    }

    let msg = '';
    switch (res.status) {
      case -1:
        msg = "API not exist";
        break;
      case -2:
        msg = "Fail";
        break;
      case -3:
        msg = "Info not exist";
        break;
      case -4:
        msg = "Account not exist";
        break;
      case -5:
        msg = "Account is locked";
        break;
      case -6:
        msg = "Giftcode is not used";
        break;
      case -7:
        msg = "Giftcode is used";
        break;
      case -8:
        msg = "Game ID is null";
        break;
      case -9:
        msg = "Sesssion ID is null";
        break;
      case -10:
        msg = "Account is not in log deposit";
        break;
      case -11:
        msg = "TxID is null";
        break;
      case -12:
        msg = "TxID is not in log deposit";
        break;
      case -13:
        msg = "Error OTP";
        break;
      case -14:
        msg = "Account is already added YEA";
        break;
      case -99:
        msg = "System Error";
        break;
      case -111:
        msg = res.desc || "Unauthorized";
        break;
      case -112:
        msg = res.desc || "No Privilege";
        break;
      default:
        msg = null;
        break;
    }
    global.showNoti({
      visible: true,
      header: "Error",
      text: msg || res.desc
    });
  }
}

export default global;