import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import global from './global.js';

Vue.use(Router)

export let router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/cash-statistics',
      name: 'cash-statistics',
      component: () => import('./views/USDTStatistics.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/cash-in',
      name: 'cash-in',
      component: () => import('./views/CashIn.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/cash-out',
      name: 'cash-out',
      component: () => import('./views/CashOut.vue'),
      meta: { requiresAuth: true }
    },
    // {
    //   path: '/pending',
    //   name: 'pending',
    //   component: () => import('./views/PendingTransaction.vue'),
    //   meta: { requiresAuth: true }
    // },
    // {
    //   path: '/user-deposit',
    //   name: 'user-deposit',
    //   component: () => import('./views/UserDeposit.vue'),
    //   meta: { requiresAuth: true }
    // },
    {
      path: '/user-info',
      name: 'user-info',
      component: () => import('./views/UserCashflow.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/game-revenue',
      name: 'game-revenue',
      component: () => import('./views/GameRevenueReal.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/view-ccu',
      name: 'view-ccu',
      component: () => import('./views/ViewCCU.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('./views/Test.vue'),
      meta: { requiresAuth: true }
    },
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (!to.matched.some(record => record.meta.requiresAuth)) {
    next()
    return
  }

  if (!global.loggedIn()) {
    next('/')
    return
  }

  next()
})